"use strict";

function createNewUser () {
    let name = prompt("Enter your first name");
    let surName = prompt("Enter your second name");
    let birthday = prompt("Enter your date of birth dd.mm.yyyy");

    let newUser = {
        firstName: name,
        lastName: surName,
        dateBirth: birthday,
        getLogin: function () {
            let login;
            login = this.firstName.toLowerCase().substr(0,1) + this.lastName.toLowerCase();
            return login;
        },
        getAge: function (){
            const today = new Date();
            const age = today.getFullYear() - this.dateBirth.substr(6, 4);
            if (today.getMonth() < this.dateBirth.substr(3, 2) ||
                (today.getMonth() === +this.dateBirth.substr(3, 2) &&
                    today.getDate() < +this.dateBirth.substr(0, 2))) {
                return age -1;
            }
            return age;
        },
        getPassword: function () {
            let password;
            password = this.firstName.toUpperCase().substr(0,1) + this.lastName.toLowerCase() + this.dateBirth.substr(6, 4);
            return password;
        },
    };
    console.log(`Login: ${newUser.getLogin()}`);
    console.log(`User Age: ${newUser.getAge()}`);
    console.log(`Password: ${newUser.getPassword()}`);
}
createNewUser();