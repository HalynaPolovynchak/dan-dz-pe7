"use strict";

let tab = function (){
    let tabNav = document.querySelectorAll(".tabs-title"), //сделали массив из навигационного меню
    tabContent = document.querySelectorAll(".tab"), //получили массив контентов
        tabName; //доп переменая определения названия вкладки для вызова соответствующего контента
    tabNav.forEach(item=> {
        item.addEventListener("click", selectTabNav)
    })
    function selectTabNav(){
        tabNav.forEach(item=>{
            item.classList.remove("active");
        })
        this.classList.add("active");
        tabName = this.getAttribute("data-tab-name");
        // console.log(tabName); //check attribute data
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item =>{
            item.classList.contains(tabName)?
                item.classList.add("active"):
                item.classList.remove("active");
        })
    }
};
tab();
