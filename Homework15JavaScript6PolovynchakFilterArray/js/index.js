"use strict";

let array = [8, "cat", "", 41, "apple", true, 25, null,`change`, undefined, "dog", false, `65`, 0, NaN];
let typeToDelete = `number`;

function filterBy (data = array, dataType = typeToDelete) {
    let newArrayChecked = data;
    let typeOfFalse = dataType;
    // console.log(arrayCheck); // verification first version array
    newArrayChecked = newArrayChecked.filter(item => {
        return (typeof item !== typeOfFalse);
    });
    console.log(newArrayChecked);
    return newArrayChecked;
}

filterBy(array, typeToDelete);

// 2 example
// let data = ["const", "", 105, "get", true, 80, `96`, null, undefined, "dog", `42 monkeys`, false, `65`, 0, NaN];
// let dataType = `string`;
// filterBy(data, dataType);

// 3 example
// let data = ["const", "", 105, "get", true, 80, `96`, null, undefined, "dog", `42 monkeys`, false, `65`, 0, NaN];
// let dataType = `boolean`;
// filterBy(data, dataType);

// 4 example
// let data = ["const", "", 105, "get", true, 80, `96`, null, undefined, "dog", `42 monkeys`, false, `65`, 0, NaN];
// let dataType = `object`;
// filterBy(data, dataType);

// 5 example
// let data = ["const", "", 105, "get", true, 80, `96`, null, undefined, "dog", `42 monkeys`, false, `65`, 0, NaN];
// let dataType = `undefined`;
// filterBy(data, dataType);