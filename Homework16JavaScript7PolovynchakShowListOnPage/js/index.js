"use strict";

const root = document.getElementById("root");
let arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function makeListFromArray (a, b = document.body) {
    const elemsArray = a.map((e) => {
    const liElem = document.createElement("li");
    liElem.textContent = `${e}`;
    return liElem;
});
const fragBlock = document.createDocumentFragment();
elemsArray.forEach((e) => {
    fragBlock.append(e);
});
root.append(fragBlock);
}

// arr = ["1", "2", "3", "sea", "user", 23]; // Different arrays for example
// arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]; // Different arrays for example
// arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]; // Different arrays for example
makeListFromArray(arr);

let count = 3;
const counter=setInterval(timer, 1000); //1000 it means - will  run it every 1 second
function timer()
{
    setTimeout(() => {
        clearInterval(counter);
        return document.body.innerHTML = '';
        }, 3000);
    document.getElementById("timerEnd").innerHTML= `Осталось: ${count} second`;
    timerEnd.style.cssText = `height: auto; width: auto; background-color: yellow; text-align: center`;
    count = count - 1;
}







// FOR PERSONAL USING
// function clearPage() {
//     document.write('Страница очищена');
// }
// return clearPage();

// if (!(Array.isArray(e))) {
//     liElem.textContent = `${e}`;
//     console.log(e);
//     console.log(typeof e);
//     return liElem;
// } else {
// }